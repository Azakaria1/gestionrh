package com.rh.employeeservice.repository;

import com.rh.employeeservice.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface RoleRepo extends JpaRepository<Role, Long> {
    Role findByNom(String nom);
}
