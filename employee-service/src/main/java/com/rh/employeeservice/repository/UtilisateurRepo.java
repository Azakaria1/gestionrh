package com.rh.employeeservice.repository;

import com.rh.employeeservice.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UtilisateurRepo extends JpaRepository<Utilisateur, Long> {
    Utilisateur getUtilisateurById(Long utilisateurId);

    @Query(value = "SELECT poste_id from utilisateur where id =?1",nativeQuery = true)
    Long UserPosteId(Long id_user);
}
