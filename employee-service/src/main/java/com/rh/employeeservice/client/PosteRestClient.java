package com.rh.employeeservice.client;

import com.rh.employeeservice.model.Poste;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "job-dept-service")
public interface PosteRestClient {

    @GetMapping("/postes/{posteId}")
    Poste getPosteById(@PathVariable Long posteId);
}
