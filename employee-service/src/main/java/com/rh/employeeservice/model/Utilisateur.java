package com.rh.employeeservice.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Entity
@Table
@RequiredArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nom;

    private String prenom;

    @Column(name = "email", length = 60, nullable = false)
    @Email
    private String email;

    private Integer age;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @Transient
    private Poste poste;

    private Long posteId;

}
