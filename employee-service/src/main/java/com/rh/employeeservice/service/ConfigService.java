package com.rh.employeeservice.service;

import com.rh.employeeservice.model.Utilisateur;
import com.rh.employeeservice.repository.UtilisateurRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ConfigService {

    @Value("${server.port}")
    private int port;

    @GetMapping("/port")
    public int myConfig() {
        return port;
    }

}
