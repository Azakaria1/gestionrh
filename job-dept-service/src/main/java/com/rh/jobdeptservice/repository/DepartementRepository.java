package com.rh.jobdeptservice.repository;

import com.rh.jobdeptservice.model.Departement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface DepartementRepository extends JpaRepository<Departement,Long> {

    Departement getDepartementById(Long departementId);
    Departement getDepartementByNom( String nom);
}
