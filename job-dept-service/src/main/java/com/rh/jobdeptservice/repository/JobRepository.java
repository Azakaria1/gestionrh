package com.rh.jobdeptservice.repository;

import com.rh.jobdeptservice.model.Poste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface JobRepository extends JpaRepository<Poste,Long> {

    Poste getPosteById(Long posteId);
}
