package com.rh.jobdeptservice.DTO;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PosteDTO {

    private String intitule;

    private LocalDate date_decrochage;

    private DepartementDTO departementDto;


}
