package com.rh.jobdeptservice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Entity
@Table
@RequiredArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Poste {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String intitule;

    private LocalDate date_decrochage;

    @OneToOne
    private Departement departement;


}