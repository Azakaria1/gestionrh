package com.rh.jobdeptservice.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table
@RequiredArgsConstructor
@AllArgsConstructor
@Data@Builder
public class Departement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    private String nom;

    @OneToMany(fetch = FetchType.EAGER)
    @ToString.Exclude
    private List<Poste> postes ;
}
