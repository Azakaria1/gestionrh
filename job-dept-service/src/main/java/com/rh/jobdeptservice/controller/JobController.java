package com.rh.jobdeptservice.controller;

import com.rh.jobdeptservice.DTO.PosteDTO;
import com.rh.jobdeptservice.model.Poste;
import com.rh.jobdeptservice.service.JobService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class JobController {

    private final JobService jobService;

    @GetMapping("/postes/user/{id}")
    public List<Poste> getPostesForUser(@PathVariable("id") String userId) {

        /*
        List<Poste> roles = jobService.getRolesForUser(userId);
        return roles;
        */
        return null;
    }

    @GetMapping("/postes/{posteId}")
    public ResponseEntity<Poste> getPosteById(@PathVariable Long posteId) {
        return ResponseEntity.ok().body(jobService.getPosteById(posteId));
    }

    @PostMapping("/ajouter-poste")
    public ResponseEntity<Poste> addPoste(@RequestBody PosteDTO posteDTO) {
        return ResponseEntity.ok().body(jobService.save(posteDTO));
    }
}
