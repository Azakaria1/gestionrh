package com.rh.jobdeptservice.controller;
import com.rh.jobdeptservice.DTO.DepartementDTO;
import com.rh.jobdeptservice.model.Departement;
import com.rh.jobdeptservice.service.DepartementService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/departements")
public class DepartementController {

    private final DepartementService departementService;

    @GetMapping("/{departementId}")
    public ResponseEntity<Departement> getDepartementById(@PathVariable Long departementId) {
        return ResponseEntity.ok().body(departementService.getDepartementById(departementId));
    }

    @GetMapping()
    public ResponseEntity<List<Departement>> getDepartements() {
        return ResponseEntity.ok().body(departementService.findAll());
    }

    @PostMapping("/addDepartement")
    public ResponseEntity<Departement> addDepartement(@RequestBody DepartementDTO departementDTO) {
        return ResponseEntity.ok().body(departementService.save(departementDTO));
    }
}
