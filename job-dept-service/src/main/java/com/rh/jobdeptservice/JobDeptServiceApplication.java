package com.rh.jobdeptservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class JobDeptServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobDeptServiceApplication.class, args);
	}

}
