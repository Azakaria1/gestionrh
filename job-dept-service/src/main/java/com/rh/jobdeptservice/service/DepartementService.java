package com.rh.jobdeptservice.service;


import com.rh.jobdeptservice.DTO.DepartementDTO;
import com.rh.jobdeptservice.model.Departement;
import com.rh.jobdeptservice.repository.DepartementRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartementService {

    private final DepartementRepository departementRepository;
    private final ModelMapper modelMapper = new ModelMapper();

    public Departement getDepartementById(Long departementId) {
        return departementRepository.getDepartementById(departementId);
    }

    public Departement save(DepartementDTO departementDTO) {

        return departementRepository.save( modelMapper.map(departementDTO, Departement.class) );
    }

    public List<Departement> findAll() {
        return departementRepository.findAll();
    }
}
