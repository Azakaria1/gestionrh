package com.rh.jobdeptservice.service;

import com.rh.jobdeptservice.DTO.PosteDTO;
import com.rh.jobdeptservice.model.Departement;
import com.rh.jobdeptservice.model.Poste;
import com.rh.jobdeptservice.repository.DepartementRepository;
import com.rh.jobdeptservice.repository.JobRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobService {

    private final JobRepository jobRepository;
    private final DepartementRepository departementRepository;
    private final ModelMapper modelMapper = new ModelMapper();

    public Poste getPosteById(Long posteId) {
        return jobRepository.getPosteById(posteId);
    }

    public Poste save(PosteDTO posteDTO) {
        Poste poste = modelMapper.map(posteDTO, Poste.class);
        poste.setDepartement(departementRepository.getDepartementByNom(poste.getDepartement().getNom()));
        System.out.println(poste);
        Departement departement = poste.getDepartement();
        departement.getPostes().add(poste);
        Poste poste1 = jobRepository.save( poste );
        departementRepository.save(departement);
        return poste1;
    }

}
