package com.rh.authservice.security.service;

import com.rh.authservice.model.Utilisateur;
import com.rh.authservice.repository.RoleRepo;
import com.rh.authservice.repository.UtilisateurRepo;
import com.rh.authservice.security.auth.AuthenticationRequest;
import com.rh.authservice.security.auth.AuthenticationResponse;
import com.rh.authservice.security.auth.RegisterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UtilisateurRepo utilisateurRepo;
    private final RoleRepo roleRepo;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse register(RegisterRequest registerRequest) {

        var utilisateur = Utilisateur
                .builder()
                .age(registerRequest.getAge())
                .nom(registerRequest.getNom())
                .prenom(registerRequest.getPrenom())
                .email(registerRequest.getEmail())
                .password(passwordEncoder.encode( registerRequest.getPassword() ))
                .role(roleRepo.findByNom("USER"))
                .build();
        utilisateurRepo.save(utilisateur);

        var jwtToken = jwtService.generateToken(utilisateur);

        System.err.println("token => " + jwtToken);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

    public AuthenticationResponse login(AuthenticationRequest authenticationRequest) {
        authenticationManager.authenticate( new UsernamePasswordAuthenticationToken(  authenticationRequest.getEmail() ,authenticationRequest.getPassword()) );

        var utilisateur = utilisateurRepo.findByEmail(authenticationRequest.getEmail());

        var jwtToken = jwtService.generateToken(utilisateur);

        System.err.println("token => " + jwtToken);

        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
