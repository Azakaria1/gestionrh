package com.rh.authservice.repository;

import com.rh.authservice.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UtilisateurRepo extends JpaRepository<Utilisateur, Long> {
    Utilisateur getUtilisateurById(Long utilisateurId);
    Utilisateur findByEmail(String email);

    @Query(value = "SELECT poste_id from utilisateur where id =?1",nativeQuery = true)
    Long UserPosteId(Long id_user);
}
