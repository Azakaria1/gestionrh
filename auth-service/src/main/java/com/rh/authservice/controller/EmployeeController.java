
package com.rh.authservice.controller;

import com.rh.authservice.model.Poste;
import com.rh.authservice.model.Utilisateur;
import com.rh.authservice.repository.UtilisateurRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class EmployeeController {

    private final UtilisateurRepo utilisateurRepo;

    @PostMapping("/utilisateurs/{utilisateurId}/ajouter-poste")
    public ResponseEntity<String> ajouter_Poste_A_Utilisateur(@PathVariable Long utilisateurId/*, @RequestBody AjoutPosteDTO ajoutPosteDTO*/ ) {

        Utilisateur utilisateur = utilisateurRepo.getUtilisateurById(utilisateurId);

        utilisateur.setPosteId(/*ajoutPosteDTO.getPosteId()*/ null);

        utilisateurRepo.save(utilisateur);
        return ResponseEntity.ok("Poste ajouté à l'utilisateur avec succès.");
    }

    @GetMapping("/utilisateurs/{utilisateurId}/poste")
    public ResponseEntity<Poste> getUserPoste(@PathVariable Long utilisateurId) {

/*
        Utilisateur utilisateur = utilisateurRepo.getUtilisateurById(utilisateurId);
*/

        Long idposte = utilisateurRepo.UserPosteId(utilisateurId);

        return ResponseEntity.ok().body(/*posteRestClient.getPosteById(idposte)*/ null);
    }

    @GetMapping("/utilisateurs")
    public List<Utilisateur> getUsers() {

        return  utilisateurRepo.findAll();
    }
}
