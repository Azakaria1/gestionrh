package com.rh.congeabsenceservice.service;

import com.rh.congeabsenceservice.repo.CongeRepo;
import org.springframework.stereotype.Service;

@Service
public class CongeService {
    private final CongeRepo congeRepo;

    public CongeService(CongeRepo congeRepo) {
        this.congeRepo = congeRepo;
    }
}
