package com.rh.congeabsenceservice.service;

import com.rh.congeabsenceservice.repo.AbsenceRepo;
import org.springframework.stereotype.Service;

@Service
public class AbsenceService {
    private final AbsenceRepo absenceRepo;

    public AbsenceService(AbsenceRepo absenceRepo) {
        this.absenceRepo = absenceRepo;
    }
}
