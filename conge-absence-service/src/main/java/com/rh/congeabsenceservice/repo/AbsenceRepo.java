package com.rh.congeabsenceservice.repo;

import com.rh.congeabsenceservice.model.Absence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AbsenceRepo extends JpaRepository<Absence, Long> {
}
