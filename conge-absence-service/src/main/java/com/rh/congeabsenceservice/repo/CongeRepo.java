package com.rh.congeabsenceservice.repo;

import com.rh.congeabsenceservice.model.Conge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CongeRepo extends JpaRepository<Conge, Long> {
}
