package com.rh.congeabsenceservice.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Entity
@Table
@RequiredArgsConstructor
@AllArgsConstructor
@Data@Builder
public class Conge {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private LocalDate date_depart;

    private LocalDate date_arrivee;

    @Transient
    @Min(value = Conge.LIMIT_CONGE)
    private int duree;

    private  static final int LIMIT_CONGE = 18;
    public long getDuree() {
        return ChronoUnit.DAYS.between(getDate_depart(), getDate_arrivee());
    }


}
