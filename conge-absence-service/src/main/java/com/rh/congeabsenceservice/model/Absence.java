package com.rh.congeabsenceservice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Entity
@Table
@RequiredArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Absence {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int duree;

    private String motif;

    private LocalDate date_absence;
}
