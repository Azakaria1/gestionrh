package com.rh.avgsocservice.service;

import com.rh.avgsocservice.repo.AvgSocRepo;
import org.springframework.stereotype.Service;

@Service
public class AvgSocService {
    private final AvgSocRepo avgSocRepo;

    public AvgSocService(AvgSocRepo avgSocRepo) {
        this.avgSocRepo = avgSocRepo;
    }
}
