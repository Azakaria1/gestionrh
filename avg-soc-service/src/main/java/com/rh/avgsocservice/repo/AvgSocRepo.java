package com.rh.avgsocservice.repo;
import com.rh.avgsocservice.model.AvantagesSociaux;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AvgSocRepo extends JpaRepository<AvantagesSociaux, Long> {
}
