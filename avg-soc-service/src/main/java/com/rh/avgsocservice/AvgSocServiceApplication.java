package com.rh.avgsocservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class AvgSocServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvgSocServiceApplication.class, args);
	}

}
